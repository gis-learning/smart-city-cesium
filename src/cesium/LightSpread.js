import * as Cesium from "cesium";
import LightSpreadMaterialProperty from "./material/LightSpreadMaterialProperty";
import gsap from "gsap";

export default class LightSpread {
  constructor(viewer) {
    // 121.57,29.79,
    // 113.3191,23.109,
    // 设置雷达材质
    this.LightSpreadMaterial = new LightSpreadMaterialProperty(
      "LightSpreadMaterial"
    );
    this.params = {
      minlot: 121.555,
      minLat: 29.81,
      maxlot: 121.56,
      maxLat: 29.815,
    };
    this.entity = viewer.entities.add({
      rectangle: {
        coordinates: Cesium.Rectangle.fromDegrees(
          121.555,
          29.81,
          121.56,
          29.815
        ),
        material: this.LightSpreadMaterial,
      },
    });
    gsap.to(this.params, {
      minlot: 121.56,
      minLat: 29.795,
      maxlot: 121.565,
      maxLat: 29.805,
      duration: 5,
      repeat: -1,
      // yoyo: true,
      ease: "linear",
      onUpdate: () => {
        this.entity.rectangle.coordinates = Cesium.Rectangle.fromDegrees(
          this.params.minlot,
          this.params.minLat,
          this.params.maxlot,
          this.params.maxLat
        );
      },
    });
  }
}
