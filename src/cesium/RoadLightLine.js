import * as Cesium from "cesium";
import SpritelineMaterialProperty from "./material/SpritelineMaterialProperty";
export default class RoadLightLine {
  constructor(viewer) {
    let geoJsonPromise = Cesium.GeoJsonDataSource.load(
      "./geojson/roadline.geojson"
    );
    geoJsonPromise.then((dataSource) => {
      viewer.dataSources.add(dataSource);
      let entities = dataSource.entities.values;
      let spritelineMaterialProperty = new SpritelineMaterialProperty();
      entities.forEach((item) => {
        let polyline = item.polyline;
        polyline.material = spritelineMaterialProperty;
      });
    });
  }
}
