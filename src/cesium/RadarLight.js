import * as Cesium from "cesium";
import RadarMaterialProperty from "./material/RadarMaterialProperty";

export default class RadarLight {
  constructor(viewer) {
    // 121.57,29.79,
    // 设置雷达材质
    this.radarMaterial = new RadarMaterialProperty("radarMaterial");
    this.entity = viewer.entities.add({
      rectangle: {
        coordinates: Cesium.Rectangle.fromDegrees(
          121.548,
          29.794,
          121.553,
          29.799
        ),
        material: this.radarMaterial,
      },
    });
  }
}
