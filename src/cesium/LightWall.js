import * as Cesium from "cesium";
import LightWallMaterialProperty from "./material/LightWallMaterialProperty";

export default class LightWall {
  constructor(viewer) {
    // 设置雷达材质
    this.LightWallMaterial = new LightWallMaterialProperty("LightWallMaterial");
    this.entity = viewer.entities.add({
      name: "lightWall",
      position: Cesium.Cartesian3.fromDegrees(121.55, 29.8, 200.0,),
      wall: {
        positions: Cesium.Cartesian3.fromDegreesArrayHeights([
          121.545, 29.8, 200.0,
          121.555, 29.8, 200.0,
          121.555, 29.81, 200.0,
          121.545, 29.81, 200.0,
          121.545, 29.8, 200.0,
        ]),
        material: this.LightWallMaterial,
        // outline: true,
      },
      label: {
        text: "科技园光墙",
        font: "16px sans-serif",
        style: Cesium.LabelStyle.FILL_AND_OUTLINE,
        // outlineWidth: 2,
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
        pixelOffset: new Cesium.Cartesian2(0, -20),
        fillColor: Cesium.Color.WHITE,
        // outlineColor: Cesium.Color.BLACK,
      },
    });
  }
}
